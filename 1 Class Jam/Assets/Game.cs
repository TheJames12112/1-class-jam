﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game : MonoBehaviour
{
    #region Serialized Fields

    [Header("Camera")]
    [SerializeField] private Transform              m_CameraTransform;
    [SerializeField] private Transform              m_TargetTransfrom;
    [SerializeField] private float                  m_CameraHeight;
    [SerializeField] private float                  m_ZOffset;

    [Header("Player")]
    [SerializeField] private Rigidbody              m_PlayerRigidBody;
    [SerializeField] private Transform              m_PlayerSprite;
    [SerializeField] private float                  m_BaseSpeed;
    [SerializeField] private float                  m_SpeedIncreaseNSeconds;
    [SerializeField] private float                  m_SpeedIncrease;
    [SerializeField] private float[]                m_HorizontalPositions;
    [SerializeField] private float                  m_HorMoveTime;
    [SerializeField] private float                  m_PlayerJumpHeight;
    [SerializeField] private float                  m_PlayerJumpDuration;
    [SerializeField] private float                  m_PlayerJumpScale;

    [Header("Coins")]
    [SerializeField] private GameObject[]           m_CoinPool;
    [SerializeField] private Transform              m_CoinSpawnPos;
    [SerializeField] private float                  m_CoinSpawnHeight;
    [SerializeField] private Transform              m_CoinTidyUpPos;
    [SerializeField] private float                  m_MinCoinSpawnDistance;
    [SerializeField] private float                  m_MaxCoinSpawnDistance;
    [SerializeField] private int                    m_MinCoinRowLength;
    [SerializeField] private int                    m_MaxCoinRowLength;
    [SerializeField] private float                  m_DistanceBetweenCoins;
    [SerializeField] private float                  m_CoinSafeSpawnDistance;

    [Header("Barrels")]
    [SerializeField] private GameObject[]           m_BarrelPool;
    [SerializeField] private Transform              m_BarrelSpawnPos;
    [SerializeField] private float                  m_BarrelSpawnHeight;
    [SerializeField] private Transform              m_BarrelTidyUpPos;
    [SerializeField] private float                  m_MinBarrelSpawnDistance;
    [SerializeField] private float                  m_MaxBarrelSpawnDistance;
    [SerializeField] private float                  m_BarrelIncreaseNMeters;
    [SerializeField] private float                  m_BarrleDistanceReduction;

    [Header("Cactus")]
    [SerializeField] private GameObject[]           m_CactusPool;
    [SerializeField] private Sprite[]               m_CactusImages;
    [SerializeField] private Transform              m_CactusSpawnLeft;
    [SerializeField] private Transform              m_CactusSpawnRight;
    [SerializeField] private float                  m_CactusSpawnHeight;
    [SerializeField] private float                  m_CactusSpawnXVariation;
    [SerializeField] private Transform              m_CactusTidyUpPos;
    [SerializeField] private float                  m_MinCactusSpawnDistance;
    [SerializeField] private float                  m_MaxCactusSpawnDistance;

    [Header("Rocks")]
    [SerializeField] private GameObject[]           m_RockPool;
    [SerializeField] private Sprite[]               m_RockImages;
    [SerializeField] private Transform              m_RockSpawnLeft;
    [SerializeField] private Transform              m_RockSpawnRight;
    [SerializeField] private float                  m_RockSpawnHeight;
    [SerializeField] private float                  m_RockSpawnXVariation;
    [SerializeField] private Transform              m_RockTidyUpPos;
    [SerializeField] private float                  m_MinRockSpawnDistance;
    [SerializeField] private float                  m_MaxRockSpawnDistance;

    [Header("Road Scrolling")]
    [SerializeField] private Transform[]            m_RoadTiles;
    [SerializeField] private float                  m_TileSize;
    [SerializeField] private float                  m_ViewZone;

    [Header("Gui")]
    [SerializeField] private RectTransform          m_MainMenuUI;
    [SerializeField] private RectTransform          m_InGameUI;
    [SerializeField] private RectTransform          m_GameOverUI;
    [SerializeField] private RectTransform          m_GameOverButtonGroup;
    [SerializeField] private Text                   m_DistanceText;
    [SerializeField] private Text                   m_CoinText;
    [SerializeField] private Text                   m_GameOverCoinText;
    [SerializeField] private Text                   m_GameOverXText;
    [SerializeField] private Text                   m_GameOverDistanceText;
    [SerializeField] private Text                   m_FinalScoreText;

    [Header("Audio")]
    [SerializeField] private AudioSource            m_OneShotSource;
    [SerializeField] private AudioClip              m_UISelectClip;
    [SerializeField] private float                  m_UIClipVolume;
    [SerializeField] private AudioClip              m_CoinCollectClip;
    [SerializeField] private float                  m_CoinClipVolume;
    [SerializeField] private AudioClip              m_BarrelHitClip;
    [SerializeField] private float                  m_BarrelClipVolume;
    [SerializeField] private AudioClip              m_PlayerJumpClip;
    [SerializeField] private float                  m_JumpClipVolume;
    [SerializeField] private AudioClip              m_PlayerLandClip;
    [SerializeField] private float                  m_LandClipVolume;
    [SerializeField] private AudioClip              m_FinalScoreClip;
    [SerializeField] private float                  m_ScoreClipVolume;

    #endregion

    #region Private Vars

    private int                 m_TopIndex;
    private int                 m_BottomIndex;
    private int                 m_HorPositionIndex = 1;

    private Vector3             m_StartPos;

    private float               m_DistanceTravelled;
    private int                 m_CoinsCollected;
    private float               m_GameStartTime;
    private float               m_CurrentGameTime;

    private IEnumerator         m_PlayerHorRoutine;
    private IEnumerator         m_PlayerJumpRoutine;
    private IEnumerator         m_CoinSpawnRoutine;
    private IEnumerator         m_BarrelSpawnRoutine;
    private IEnumerator         m_CactusSpawnRoutine;
    private IEnumerator         m_RockSpawnRoutine;
    private IEnumerator         m_FinalScoreRoutine;

    private int                 m_GameState = 0;

    #endregion

    #region Unity Functions

    private void Start ()
    {
        m_TopIndex = 0;
        m_BottomIndex = m_RoadTiles.Length - 1;

        m_StartPos = transform.position;
	}
	
	private void Update ()
    {
		switch(m_GameState)
        {
            case 0:
                break;

            case 1:
                HandleInput();
                MovePlayerForward();
                MoveCamera();
                ScrollRoad();
                UpdateGameValues();
                UpdateUI();
                TidyUpOldCoins();
                TidyUpOldBarrels();
                TidyUpOldCactuses();
                TidyUpOldRocks();
                break;

            case 2:
                break;
        }
	}

    private void FixedUpdate()
    {
        switch (m_GameState)
        {
            case 0:
                break;

            case 1:
                MovePlayerForward();
                break;

            case 2:
                break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        string tag = other.gameObject.tag;

        switch(tag)
        {
            case "Coin":
                CoinCollect(other.gameObject);
                break;

            case "Barrel":
                BarrelHit();
                break;
        }
    }

    #endregion

    #region Core Functions

    private void StartGame()
    {
        m_MainMenuUI.gameObject.SetActive(false);
        m_InGameUI.gameObject.SetActive(true);
        m_GameStartTime = Time.time;

        m_CoinSpawnRoutine = CoinSpawnRoutine();
        StartCoroutine(m_CoinSpawnRoutine);

        m_BarrelSpawnRoutine = BarrelSpawnRoutine();
        StartCoroutine(m_BarrelSpawnRoutine);

        m_CactusSpawnRoutine = CactusSpawnRoutine();
        StartCoroutine(m_CactusSpawnRoutine);

        m_RockSpawnRoutine = RockSpawnRoutine();
        StartCoroutine(m_RockSpawnRoutine);

        m_GameState = 1;
    }

    private void GameOver()
    {
        m_GameState = 2;

        StopCoroutine(m_CoinSpawnRoutine);
        m_CoinSpawnRoutine = null;
        StopCoroutine(m_BarrelSpawnRoutine);
        m_BarrelSpawnRoutine = null;
        StopCoroutine(m_CactusSpawnRoutine);
        m_CactusSpawnRoutine = null;
        StopCoroutine(m_RockSpawnRoutine);
        m_RockSpawnRoutine = null;

        if(m_PlayerHorRoutine != null)
        {
            StopCoroutine(m_PlayerHorRoutine);
            m_PlayerHorRoutine = null;
        }

        if (m_PlayerJumpRoutine != null)
        {
            StopCoroutine(m_PlayerJumpRoutine);
            m_PlayerJumpRoutine = null;
        }

        m_PlayerRigidBody.velocity = Vector3.zero;

        m_FinalScoreRoutine = CalculateFinalScoreRoutine();
        StartCoroutine(m_FinalScoreRoutine);
    }

    private void RestartGame()
    {
        Vector3 playerPos = m_PlayerRigidBody.position;
        playerPos.x = m_HorizontalPositions[1];
        m_PlayerRigidBody.position = playerPos;
        m_HorPositionIndex = 1;

        m_StartPos = transform.position;
        m_GameOverUI.gameObject.SetActive(false);
        m_DistanceTravelled = 0.0f;
        m_CoinsCollected = 0;

        ResetCoins();
        ResetBarrels();
        ResetCactuses();
        ResetRocks();

        StartGame();
    }

    private void UpdateGameValues()
    {
        m_DistanceTravelled = transform.position.z - m_StartPos.z;
        m_CurrentGameTime = Time.time - m_GameStartTime;
    }

    private void HandleInput()
    {
        if(Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
        {
            MovePlayerLeft();
        }

        if(Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
        {
            MovePlayerRight();
        }

        if(Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Space))
        {
            PlayerJump();
        }
    }

    private void MoveCamera()
    {
        Vector3 targetPos = m_CameraTransform.position;
        targetPos.y = m_CameraHeight;
        targetPos.z = m_TargetTransfrom.position.z + m_ZOffset;
        m_CameraTransform.position = targetPos;
    }

    #endregion

    #region Player Movement

    private void MovePlayerForward()
    {
        float multiplier = Mathf.Floor(m_CurrentGameTime / m_SpeedIncreaseNSeconds);
        float speed = m_BaseSpeed + (m_SpeedIncrease * multiplier);
        m_PlayerRigidBody.velocity = new Vector3(0, 0, speed);
    }

    private void MovePlayerLeft()
    {
        if(m_HorPositionIndex > 0)
        {
            m_HorPositionIndex--;

            if(m_PlayerHorRoutine != null)
            {
                StopCoroutine(m_PlayerHorRoutine);
            }

            m_PlayerHorRoutine = MovePlayerHorizontal(m_HorizontalPositions[m_HorPositionIndex], m_HorMoveTime);
            StartCoroutine(m_PlayerHorRoutine);
        }
    }

    private void MovePlayerRight()
    {
        if(m_HorPositionIndex < m_HorizontalPositions.Length - 1)
        {
            m_HorPositionIndex++;

            if (m_PlayerHorRoutine != null)
            {
                StopCoroutine(m_PlayerHorRoutine);
            }

            m_PlayerHorRoutine = MovePlayerHorizontal(m_HorizontalPositions[m_HorPositionIndex], m_HorMoveTime);
            StartCoroutine(m_PlayerHorRoutine);
        }
    }

    private IEnumerator MovePlayerHorizontal(float targetX, float duration)
    {
        float startX = m_PlayerRigidBody.position.x;

        float t = 0.0f;
        while(t < 1)
        {
            t += Time.deltaTime / duration;
            float currnetX = Mathf.Lerp(startX, targetX, t);
            Vector3 newPos = m_PlayerRigidBody.position;
            newPos.x = currnetX;
            m_PlayerRigidBody.position = newPos;

            yield return null;
        }

        Vector3 finalPos = m_PlayerRigidBody.position;
        finalPos.x = targetX;
        m_PlayerRigidBody.position = finalPos;

        m_PlayerHorRoutine = null;
    }

    private void PlayerJump()
    {
        if(m_PlayerJumpRoutine == null)
        {
            m_PlayerJumpRoutine = PlayerJumpRoutine();
            StartCoroutine(m_PlayerJumpRoutine);
        }
    }

    private IEnumerator PlayerJumpRoutine()
    {
        bool landPlayed = false;
        m_OneShotSource.PlayOneShot(m_PlayerJumpClip, m_JumpClipVolume);

        float t = 0;
        while(t < 1)
        {
            t += Time.deltaTime / m_PlayerJumpDuration;

            float t2;
            if(t < 0.5f)
            {
                t2 = Mathf.Lerp(0.0f, 1.0f, t);
            }
            else
            {
                t2 = Mathf.Lerp(1.0f, 0.0f, t);
            }

            Vector3 playerPos = m_PlayerRigidBody.position;
            playerPos.y = Mathf.Lerp(m_StartPos.y, m_StartPos.y + m_PlayerJumpHeight, t2);
            m_PlayerRigidBody.position = playerPos;

            float scale = Mathf.Lerp(1, m_PlayerJumpScale, t2);
            Vector3 playerScale = new Vector3(scale, scale, scale);
            m_PlayerSprite.localScale = playerScale;

            if (t > 0.9f && !landPlayed)
            {
                m_OneShotSource.PlayOneShot(m_PlayerLandClip, m_LandClipVolume);
                landPlayed = true;
            }

            yield return null;
        }

        Vector3 finalPos = m_PlayerRigidBody.position;
        finalPos.y = m_StartPos.y;
        m_PlayerRigidBody.position = finalPos;

        m_PlayerSprite.localScale = Vector3.one;

        m_PlayerJumpRoutine = null;
    }

    #endregion

    #region Coin Management

    private void CoinCollect(GameObject coin)
    {
        m_OneShotSource.PlayOneShot(m_CoinCollectClip, m_CoinClipVolume);
        coin.SetActive(false);
        m_CoinsCollected++;
    }

    private IEnumerator CoinSpawnRoutine()
    {
        Vector3 lastCoinSpawnPos = Vector3.zero;

        while (true)
        {
            float horPos = m_HorizontalPositions[Random.Range(0, m_HorizontalPositions.Length)];
            int coinCount = Random.Range(m_MinCoinRowLength, m_MaxCoinRowLength);

            for(int i = 0; i < coinCount; i++)
            {
                Vector3 spawnPoint = m_CoinSpawnPos.position;
                spawnPoint.x = horPos;
                spawnPoint.y = m_CoinSpawnHeight;
                spawnPoint.z += i * m_DistanceBetweenCoins;
                SpawnCoinFromPool(spawnPoint);

                if(i == coinCount - 1)
                {
                    lastCoinSpawnPos = spawnPoint;
                }
            }

            CheckCoinSafeDistance();
            float newSpawnPointZ = Random.Range(m_MinCoinSpawnDistance, m_MaxCoinSpawnDistance);
            
            while(m_CoinSpawnPos.position.z < lastCoinSpawnPos.z + newSpawnPointZ)
            {
                yield return null;
            }
        }
    }

    private void SpawnCoinFromPool(Vector3 pos)
    {
        bool coinSpawned = false;

        for(int i = 0; i < m_CoinPool.Length; i++)
        {
            if(!m_CoinPool[i].activeInHierarchy)
            {
                m_CoinPool[i].transform.position = pos;
                m_CoinPool[i].SetActive(true);
                coinSpawned = true;
                break;
            }
        }

        if(!coinSpawned)
        {
            Debug.LogWarning("Coin pool is not large enough, unable to spawn new coin");
        }
    }

    private void CheckCoinSafeDistance()
    {
        for (int i = 0; i < m_CoinPool.Length; i++)
        {
            if (m_CoinPool[i].activeInHierarchy)
            {
                for (int j = 0; j < m_BarrelPool.Length; j++)
                {
                    if (m_BarrelPool[j].activeInHierarchy)
                    {
                        if (Vector3.Distance(m_CoinPool[i].transform.position, m_BarrelPool[j].transform.position) < m_CoinSafeSpawnDistance)
                        {
                            m_CoinPool[i].SetActive(false);
                        }
                    }
                }
            }
        }
    }

    private void TidyUpOldCoins()
    {
        for(int i = 0; i < m_CoinPool.Length; i++)
        {
            if (m_CoinPool[i].activeInHierarchy)
            {
                if (m_CoinPool[i].transform.position.z < m_CoinTidyUpPos.position.z)
                {
                    m_CoinPool[i].SetActive(false);
                }
            }
        }
    }

    private void ResetCoins()
    {
        for(int i = 0; i < m_CoinPool.Length; i++)
        {
            m_CoinPool[i].SetActive(false);
        }
    }

    #endregion

    #region Barrel Management

    private void BarrelHit()
    {
        m_OneShotSource.PlayOneShot(m_BarrelHitClip, m_BarrelClipVolume);
        GameOver();
    }

    private IEnumerator BarrelSpawnRoutine()
    {
        Vector3 lastBarrelSpawnPos = Vector3.zero;

        while(true)
        {
            int barrelCount = Random.Range(1, m_HorizontalPositions.Length + 1);

            int startHorIndex = 0;
            switch(barrelCount)
            {
                case 1:
                    startHorIndex = Random.Range(0, m_HorizontalPositions.Length);
                    break;

                case 2:
                    startHorIndex = Random.Range(0, m_HorizontalPositions.Length - 1);
                    break;

                case 3:
                    startHorIndex = 0;
                    break;
            }
            
            for(int i = 0; i < barrelCount; i++)
            {
                Vector3 spawnPos = m_BarrelSpawnPos.position;
                spawnPos.x = m_HorizontalPositions[startHorIndex + i];
                spawnPos.y = m_BarrelSpawnHeight;
                SpawnBarrelFromPool(spawnPos);
            }

            CheckCoinSafeDistance();
            lastBarrelSpawnPos = m_BarrelSpawnPos.position;

            float maxDistance = m_MaxBarrelSpawnDistance - (Mathf.FloorToInt(m_DistanceTravelled / m_BarrelIncreaseNMeters) * m_BarrleDistanceReduction);

            if(maxDistance < m_MinBarrelSpawnDistance)
            {
                maxDistance = m_MinBarrelSpawnDistance;
            }

            float newSpawnPointZ = Random.Range(m_MinBarrelSpawnDistance, maxDistance);

            while (m_CoinSpawnPos.position.z < lastBarrelSpawnPos.z + newSpawnPointZ)
            {
                yield return null;
            }
        }
    }

    private void SpawnBarrelFromPool(Vector3 pos)
    {
        bool barrelSpawned = false;

        for(int i = 0; i < m_BarrelPool.Length; i++)
        {
            if(!m_BarrelPool[i].activeInHierarchy)
            {
                m_BarrelPool[i].transform.position = pos;
                m_BarrelPool[i].SetActive(true);
                barrelSpawned = true;
                break;
            }
        }

        if(!barrelSpawned)
        {
            Debug.LogWarning("Barrel pool is not large enough, unable to spawn new Barrel");
        }
    }

    private void TidyUpOldBarrels()
    {
        for (int i = 0; i < m_BarrelPool.Length; i++)
        {
            if (m_BarrelPool[i].activeInHierarchy)
            {
                if (m_BarrelPool[i].transform.position.z < m_BarrelTidyUpPos.position.z)
                {
                    m_BarrelPool[i].SetActive(false);
                }
            }
        }
    }

    private void ResetBarrels()
    {
        for(int i = 0; i < m_BarrelPool.Length; i++)
        {
            m_BarrelPool[i].SetActive(false);
        }
    }

    #endregion

    #region Cactus Management

    private IEnumerator CactusSpawnRoutine()
    {
        Vector3 lastCactusSpawnPos = Vector3.zero;

        while(true)
        {
            Vector3 leftPos = m_CactusSpawnLeft.position;
            Vector3 rightPos = m_CactusSpawnRight.position;
            leftPos.y = m_CactusSpawnHeight;
            rightPos.y = m_CactusSpawnHeight;
            leftPos.x += -transform.position.x + Random.Range(-m_CactusSpawnXVariation, m_CactusSpawnXVariation);
            rightPos.x += -transform.position.x + Random.Range(-m_CactusSpawnXVariation, m_CactusSpawnXVariation);

            Sprite leftSprite = m_CactusImages[Random.Range(0, m_CactusImages.Length)];
            Sprite rightSprite = m_CactusImages[Random.Range(0, m_CactusImages.Length)];

            SpawnCactusFromPool(leftPos, leftSprite);
            SpawnCactusFromPool(rightPos, rightSprite);

            lastCactusSpawnPos = m_CactusSpawnLeft.position;
            float newSpawnPointZ = Random.Range(m_MinCactusSpawnDistance, m_MaxBarrelSpawnDistance);

            while (m_CoinSpawnPos.position.z < lastCactusSpawnPos.z + newSpawnPointZ)
            {
                yield return null;
            }
        }
    }

    private void SpawnCactusFromPool(Vector3 pos, Sprite sprite)
    {
        bool cactusSpawned = false;

        for (int i = 0; i < m_CactusPool.Length; i++)
        {
            if (!m_CactusPool[i].activeInHierarchy)
            {
                m_CactusPool[i].transform.position = pos;
                m_CactusPool[i].GetComponent<SpriteRenderer>().sprite = sprite;
                m_CactusPool[i].SetActive(true);
                cactusSpawned = true;
                break;
            }
        }

        if (!cactusSpawned)
        {
            Debug.LogWarning("Cactus pool is not large enough, unable to spawn new Cactus");
        }
    }

    private void TidyUpOldCactuses()
    {
        for (int i = 0; i < m_CactusPool.Length; i++)
        {
            if (m_CactusPool[i].activeInHierarchy)
            {
                if (m_CactusPool[i].transform.position.z < m_CactusTidyUpPos.position.z)
                {
                    m_CactusPool[i].SetActive(false);
                }
            }
        }
    }

    private void ResetCactuses()
    {
        for (int i = 0; i < m_CactusPool.Length; i++)
        {
            m_CactusPool[i].SetActive(false);
        }
    }

    #endregion

    #region Rock Management

    private IEnumerator RockSpawnRoutine()
    {
        Vector3 lastRockSpawnPos = Vector3.zero;

        while (true)
        {
            Vector3 leftPos = m_RockSpawnLeft.position;
            Vector3 rightPos = m_RockSpawnRight.position;
            leftPos.y = m_RockSpawnHeight;
            rightPos.y = m_RockSpawnHeight;
            leftPos.x += -transform.position.x + Random.Range(-m_RockSpawnXVariation, m_RockSpawnXVariation);
            rightPos.x += -transform.position.x + Random.Range(-m_RockSpawnXVariation, m_RockSpawnXVariation);

            Sprite leftSprite = m_RockImages[Random.Range(0, m_RockImages.Length)];
            Sprite rightSprite = m_RockImages[Random.Range(0, m_RockImages.Length)];

            SpawnRockFromPool(leftPos, leftSprite);
            SpawnRockFromPool(rightPos, rightSprite);

            lastRockSpawnPos = m_RockSpawnLeft.position;
            float newSpawnPointZ = Random.Range(m_MinRockSpawnDistance, m_MaxBarrelSpawnDistance);

            while (m_CoinSpawnPos.position.z < lastRockSpawnPos.z + newSpawnPointZ)
            {
                yield return null;
            }
        }
    }

    private void SpawnRockFromPool(Vector3 pos, Sprite sprite)
    {
        bool RockSpawned = false;

        for (int i = 0; i < m_RockPool.Length; i++)
        {
            if (!m_RockPool[i].activeInHierarchy)
            {
                m_RockPool[i].transform.position = pos;
                m_RockPool[i].GetComponent<SpriteRenderer>().sprite = sprite;
                m_RockPool[i].SetActive(true);
                RockSpawned = true;
                break;
            }
        }

        if (!RockSpawned)
        {
            Debug.LogWarning("Rock pool is not large enough, unable to spawn new Rock");
        }
    }

    private void TidyUpOldRocks()
    {
        for (int i = 0; i < m_RockPool.Length; i++)
        {
            if (m_RockPool[i].activeInHierarchy)
            {
                if (m_RockPool[i].transform.position.z < m_RockTidyUpPos.position.z)
                {
                    m_RockPool[i].SetActive(false);
                }
            }
        }
    }

    private void ResetRocks()
    {
        for (int i = 0; i < m_RockPool.Length; i++)
        {
            m_RockPool[i].SetActive(false);
        }
    }

    #endregion

    #region Gui

    private void UpdateUI()
    {
        string distanceDisplay = Mathf.FloorToInt(m_DistanceTravelled).ToString() + " m";
        m_DistanceText.text = distanceDisplay;

        string coinDisplay = "x " + m_CoinsCollected.ToString();
        m_CoinText.text = coinDisplay;
    }

    private IEnumerator CalculateFinalScoreRoutine()
    {
        m_GameOverCoinText.text = "";
        m_GameOverXText.text = "";
        m_GameOverDistanceText.text = "";
        m_FinalScoreText.text = "";
        m_GameOverButtonGroup.gameObject.SetActive(false);
        m_GameOverUI.gameObject.SetActive(true);

        yield return new WaitForSeconds(1.0f);

        m_GameOverCoinText.text = m_CoinsCollected.ToString();
        m_OneShotSource.PlayOneShot(m_FinalScoreClip, m_ScoreClipVolume);

        yield return new WaitForSeconds(1.0f);

        m_GameOverXText.text = "X";
        m_OneShotSource.PlayOneShot(m_FinalScoreClip, m_ScoreClipVolume);

        yield return new WaitForSeconds(1.0f);

        m_GameOverDistanceText.text = Mathf.FloorToInt(m_DistanceTravelled).ToString();
        m_OneShotSource.PlayOneShot(m_FinalScoreClip, m_ScoreClipVolume);

        yield return new WaitForSeconds(1.0f);

        int distance = Mathf.FloorToInt(m_DistanceTravelled);

        m_FinalScoreText.text = "0";
        if(m_CoinsCollected == 0)
        {
            m_FinalScoreText.text = Mathf.FloorToInt(m_DistanceTravelled).ToString();
        }
        {
            for (int i = 0; i < m_CoinsCollected; i++)
            {
                int score = distance * (i + 1);
                m_FinalScoreText.text = score.ToString();
                yield return null;
            }
        }

        m_GameOverButtonGroup.gameObject.SetActive(true);
        m_OneShotSource.PlayOneShot(m_FinalScoreClip, m_ScoreClipVolume);
        m_FinalScoreRoutine = null;
    }

    #endregion

    #region Tile Scrolling

    private void ScrollRoad()
    {
        if (m_CameraTransform.position.z < (m_RoadTiles[m_TopIndex].transform.position.z + m_ViewZone))
        {
            ScrolUp();
        }

        if (m_CameraTransform.position.z > (m_RoadTiles[m_BottomIndex].transform.position.z - m_ViewZone))
        {
            ScrollDown();
        }
    }

    private void ScrolUp()
    {
        Vector3 pos = m_RoadTiles[m_BottomIndex].position;
        pos.z = m_RoadTiles[m_TopIndex].position.z - m_TileSize;
        m_RoadTiles[m_BottomIndex].position = pos;

        m_TopIndex = m_BottomIndex;
        m_BottomIndex--;

        if (m_BottomIndex < 0)
        {
            m_BottomIndex = m_RoadTiles.Length - 1;
        }
    }

    private void ScrollDown()
    {
        Vector3 pos = m_RoadTiles[m_TopIndex].position;
        pos.z = m_RoadTiles[m_BottomIndex].position.z + m_TileSize;
        m_RoadTiles[m_TopIndex].position = pos;

        m_BottomIndex = m_TopIndex;
        m_TopIndex++;

        if (m_TopIndex == m_RoadTiles.Length)
        {
            m_TopIndex = 0;
        }
    }

    #endregion

    #region Button Functions

    public void Btn_Play()
    {
        m_OneShotSource.PlayOneShot(m_UISelectClip, m_UIClipVolume);
        StartGame();
    }

    public void Btn_Retry()
    {
        m_OneShotSource.PlayOneShot(m_UISelectClip, m_UIClipVolume);
        RestartGame();
    }

    public void Btn_Quit()
    {
        m_OneShotSource.PlayOneShot(m_UISelectClip, m_UIClipVolume);
        Application.Quit();
    }

    #endregion
}